exports.toArray = function(args, i) {
    var arr = [],
    len = args.length,
    i = i || 0;

    for(; i < len; ++i) {
        arr.push(args[i]);
    }

    return arr;
};

/**
 * merges passed objects into a new single object
 */
exports.merge = function merge() {
    var options;
    var obj = {};
    var len = arguments.length -1;
    for(;len >= 0; len--) {
        if((options = arguments[len]) != null) {
            for(prop in options) {
                if(options.hasOwnProperty(prop)) {
                    obj[prop] = options[prop];
                }
            }
        }
    }
    return obj;
}

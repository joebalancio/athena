exports.first = function(output, next) {
    var keys, list, collection, 
    data = output.parsed,
    newData = {};
    if(data) {

        // get list
        keys = Object.keys(data);
        list = data[keys[0]];

        // get list's model collection
        keys = Object.keys(list);
        keys.reverse();
        collection = list[keys[0]];
        newData[keys[0]] = collection.slice(0, 1);
        
        // override data
        output.parsed = newData;
    }

    // done
    next();
}

// modules
var http = require('http');
var url = require('url');
var events = require('events');
var util = require('util');
var toArray = require('./utils').toArray;
var merge = require('./utils').merge;
var handlers = require('./handlers');

// expose constructor
exports = module.exports = WebClient;

// expose handlers
exports.handlers = handlers;

// constructor
function WebClient(uri, partnerId) {
    this.uri = url.parse(uri);
    this.partnerId = partnerId;
};

function httpOptions(uri, method) {
    // check if uri is absolute
    var parsed = url.parse(uri);
    parsed.method = method;
    if(!parsed.protocol) {
        parsed.pathname = this.uri.pathname + parsed.pathname;
        parsed.href = this.uri.href + parsed.href;
        
        parsed = merge(parsed, this.uri);
    }

    // return formatted options
    return {
       protocol: parsed.protocol,
       port: parsed.port,
       host: parsed.hostname,
       path: parsed.pathname,
       headers: {
           Accept: 'application/json'
       }
    };
}

function execute(output, callbacks) {
    function next(err) {
        var fn = callbacks.shift();
        if(fn) {
            if(err) {
                // get to the last callback
                if(callbacks.length > 0) {
                    next(err);
                } else {
                    output.err = err;
                    fn(output);
                }
            } else {
                fn(output, next);
            }
        }
    }

    return next;
}

function httpResponseHandler(output, next) {
    var res = output.res;
    output.raw = '';

    res.on('data', function(chunk) {
        output.raw += chunk.toString('utf8');
    }).on('end', function() {
        output.parsed = JSON.parse(output.raw);
        next();
    }).on('close', function(err) {
        next(err);
    });
}

function httpRequestHandler(uri, method) {
    return function(output, next) {
        http.get(httpOptions(uri, method), function(res) {
            output.res = res;
            next();
        });
    };

}

// public methods
WebClient.prototype.get = function(uri, callback) {
    console.log('get');
    var output = {uri: uri};
    var callbacks = [];

    callbacks.push(httpRequestHandler(uri, 'get'));
    callbacks.push(httpResponseHandler);
    callbacks = callbacks.concat(toArray(arguments, 1));

    // attach default callback if none passed
    if(callbacks.length == 2) {
        callbacks.push(function(output, next) {
            console.log(output);
            next();
        });
    }

    // execute the method
    execute({uri: uri}, callbacks)();
};
WebClient.prototype.put = function(uri, data) {
    this.post(uri, data);
};
WebClient.prototype.post = function(uri, data) {
    console.log('post');
};
WebClient.prototype.delete = function(uri) {
    console.log('delete');
};

exports.create = function(host, port) {
    return new WebClient(host, port);
};



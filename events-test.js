var events = require('events');
var util = require('util');

function EventTester() {
    events.EventEmitter.call(this);
}
EventTester.prototype.__proto__ = events.EventEmitter.prototype;
//util.inherits(EventTester, events.EventEmitter);

EventTester.prototype.hello = function() {
    this.emit('helo');
}

var tester = new EventTester();
tester.on('helo', function() {
    console.log('hello world');
});
console.log(tester instanceof events.EventEmitter);
console.log(EventTester.super_ === events.EventEmitter);
console.log(tester.prototype);
console.log(EventTester.prototype);
console.log(typeof tester);

console.log('\ntest\n');
tester.hello();


function A() {
}
A.prototype.foo = function() {
    return 'adidas';
}

function B() {
}
B.prototype.__proto__ = A.prototype;

var newB = new B();
//console.log(newB.foo());

var assert = require('assert');
var webclient = require('./webclient');
var http = require('http');

//var cbspclient = new webclient.create('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1');
var cbspclient = new webclient('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1');

/*
cbspclient.get('/store/9', function(data) {
    var store = data.store[0];
    store.link.forEach(function(value, index, traversed) {
        console.log(value['@title']);
        if(value['@title'] == 'store') {
            cbspclient.get(value['@href'], function(data) {
                console.log(data);
            });
        }

    }, store);
    
    //console.log(data);
});
*/

function details(output, next) {
    var name, keys,
    data = output.parsed,
    linkLookup = [];

    // make sure data is not a list

    keys = Object.keys(data);
    name = keys[0];
    model = data[name][0];

    // process links
    if(model.link) {
        // reformat links
        model.link.forEach(function(value) {
            linkLookup[value['@title']] = value;
        });
    }

    if(linkLookup[name]) {
        cbspclient.get(linkLookup[name]['@href'], function(out) {
            output.parsed = out.parsed;
            next();
            
        });
    } else if(linkLookup[name + '.campaign']) {
        cbspclient.get(linkLookup[name + '.campaign']['@href'], function(out) {
            output.parsed = out.parsed;
            next();
            
        });
    } else {
        // shouldn't get here; assuming has links.
        next();
    }
}

function first(output, next) {
    var keys, list, collection, 
    data = output.parsed,
    newData = {};
    if(data) {

        // get list
        keys = Object.keys(data);
        list = data[keys[0]];

        // get list's model collection
        keys = Object.keys(list);
        keys.reverse();
        collection = list[keys[0]];
        newData[keys[0]] = collection.slice(0, 1);
        
        // override data
        output.parsed = newData;
    }

    // done
    next();
}

cbspclient.get('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1/campaign/4/store/list', webclient.handlers.first, details, function(response) {
    console.log(response.parsed);
});
function property(name) {
    var name = name;
    return function(response, next) {
        var propertyName = name;
        // implement property lookup
        console.log(name);
        next();

    };
}
/*
cbspclient.get('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1/campaign/4/store/list', first, details,
function(response, next) {
    var name, keys,
    data = response.data,
    linkLookup = [];

    // make sure data is not a list

    keys = Object.keys(data);
    name = keys[0];
    model = data[name][0];

    // process links
    if(model.link) {
        // reformat links
        model.link.forEach(function(value) {
            linkLookup[value['@title']] = value;
        });
    }

    name = 'store.keyword.list';
    if(linkLookup[name]) {
        cbspclient.get(linkLookup[name]['@href'], function(res) {
            model[name] = res.data;

            //response.data = res.data;
            //data.test = 'test';
            //console.log(res.data);
            next();
            
        });
    } else {
        // shouldn't get here; assuming has links.
        next();
    }
},
function(response) {

    console.log(response.data);
});
*/

/*
function done(data) {
    console.log(data);
}

function a(data, next) {
    data.a = 1;
    next();
}

function b(data, next) {
    data.b = 2;
    next();
}

function c(data, next) {
    data.c = 3;
    done(data);

}

var stack = [a, b, c];

var handle = function(data, out) {
    function next() {
        var func = stack.shift();
        if(func) {
            func(data, next); 
        }
    }
    next();
}

handle({});

*/

//cbspclient.put();
//cbspclient.post();
//cbspclient.delete();

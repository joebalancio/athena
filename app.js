
/**
 * Module dependencies.
 */

var express = require('express');

var app = express.createServer();

var WebClient = require('./lib/webclient');

var cbspClient = new WebClient('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1');

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// Routes
require('./routes/index')(app, cbspClient);

app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

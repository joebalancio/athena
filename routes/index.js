var WebClient = require('../lib/webclient');
/**
 * Module dependencies.
 */
module.exports = function(app, client) {
    app.get('/', 
    
    function(req, res, next) {
        client.get('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1/category/root/list', function(response) {
            console.log(response.parsed);

            res.local('parentCategories', response.parsed.categories);
            next();

        });
    },
    
    
    function(req, res, next){
        client.get('http://dv1-cbsp4.ebates.int:8080/cbsp/partner/1/campaign/4/store/list', WebClient.handlers.first, function(response) {
            console.log(response.parsed);

            res.local('store', response.parsed.store);
            next();

        });

    }, 
    
    
    function(req, res) {
        res.render('index', {
            title: 'Express',
            foo: 'Bar'
        });
    });
};
